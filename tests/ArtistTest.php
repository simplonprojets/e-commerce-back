<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArtistTest extends WebTestCase
{
    
    public function testGetAll() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/artist');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $item = $body[0];
        $this->assertIsInt($item['id']);
        $this->assertIsString($item['name']);
        $this->assertIsString($item['description']);
        $this->assertIsString($item['img']);
        $this->assertIsArray($item['albums']);
        $this->assertIsString($item['albums'][0]['title']);
        $this->assertIsArray($item['users']);
    }

    public function testGetOne() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/artist/5');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsInt($body['id']);
        $this->assertIsString($body['name']);
        $this->assertIsString($body['description']);
        $this->assertIsString($body['img']);
        $this->assertIsArray($body['albums']);
        $this->assertIsString($body['albums'][0]['title']);
        $this->assertIsArray($body['users']);
    }

    //Test du message d'erreur renvoyé dans le cas ou il n'y a pas d'item
    public function testGetByIdNotFound() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/artist/1000');

        $this->assertResponseStatusCodeSame(404);
    }

    public function testAddAlbum() {
        
        $client = static::createClient();

        $json = json_encode([
            "name" => "test",
            "description" => "lorem ipsum",
            "img" => "https://test.com",
            "albums" => [],
            "genre_id" => 1
        ]);
        
        $client->request('POST', '/api/album/a', content: $json);

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

    }

    public function testDelete() {
        
        $client = static::createClient();
        
        $client->request('DELETE', '/api/artist/a/2');

        $this->assertResponseStatusCodeSame(404);
        
    }

    //Test pour recupérer les artistes partageant le meme genre musical
    public function testGetArtistByGenre() {

        $client = static::createClient();

        $client->request('GET', '/api/artist/genre/2');

        $this->assertResponseIsSuccessful();
    }

}
