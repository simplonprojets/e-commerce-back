<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumTest extends WebTestCase
{
    
    public function testGetAll() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/album');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $item = $body[0];
        $this->assertIsInt($item['id']);
        $this->assertIsString($item['title']);
        $this->assertIsString($item['img']);
        $this->assertIsString($item['date']);
        new \DateTime($item['date']);
        $this->assertIsString($item['description']);
        $this->assertIsFloat($item['price']);
    }

    public function testGetOne() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/album/4');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsInt($body['id']);
        $this->assertIsString($body['title']);
        $this->assertIsString($body['img']);
        $this->assertIsString($body['date']);
        new \DateTime($body['date']);
        $this->assertIsString($body['description']);
        $this->assertIsFloat($body['price']);
    }

    //Test du message d'erreur renvoyé dans le cas ou il n'y a pas d'item
    public function testGetByIdNotFound() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/album/1000');

        $this->assertResponseStatusCodeSame(404);
    }

    //Ne fonctionne pas :(
    public function testAddAlbum() {
        
        $client = static::createClient();

        $json = json_encode([
            "id" => 9,
            "title"=> "Test",
            "img" => "https://test.com",
            "date" => "2015-07-25T02:52:47+00:00",
            "description" => "lorem ipsum",
            "price" => 99.99,
            "users" => []
        ]);
        
        $client->request('POST', '/api/album/a/1', content: $json);

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

    }

    public function testDelete() {
        
        $client = static::createClient();
        
        $client->request('DELETE', '/api/album/a/2');

        $this->assertResponseStatusCodeSame(204);;
        
    }



}
