# Bando e-commerce

Ce projet a de groupe a été réalisé avec [Hisami Stolz](https://gitlab.com/Hisami) et [Judith Sévy](https://gitlab.com/oblvda) lors de la formation : [Développeur et Développeur Web Mobile](https://simplon.co/formation/developpeur-web-et-web-mobile/11) de Simplon. Le site de e-commerce a été conçu avec React et Next.js pour la partie front et Symfony pour la partie back. Bando est un site de e-commerce spécialisé en streaming, vente de musiques et d'évenements.

## Conception
- Diagrammes de Use Case
- Digramme de classe
- Maquette

## Fonctionnalités
### Général
- Création de compte utilisateur ou artiste.
- Modification des informations concernant son compte.
### Utilisateur
- Un utilisateur peut acheter des billets pour un évenements et de la musique en format dématérialisé.
- Un utilisateur peut mettre en favoris un ou plusieurs albums.
- un utilisateur peux consulter ses commandes liées aux albums.
### Artiste
- Un artiste peut créer une page qui lui est dédié, il peut y partager sa musique.

## Aspects techniques
### Front
- Router de Next
- Props
- Pages et Composants
- Interfaces
- Requetes vers l'API Rest
- Responsive

### Back
- Entité
- Repository
- Controller
- API Rest
- Doctrine

## Ajout possible

- La barre de recherche fonctionne uniquement pour la recherche d'un artiste, elle pourrait etre améliorée avec d'autres filtres de recherches (comme le genre musical, les albums, les chansons et les évenements).
- Les pages Genres/Artistes/Évenements ne sont pas fonctionnelles
- L'upload de chansons n'est pas fonctionnel.
- La création d'évenements n'est pas fonctionnel depuis l'interface.
- L'utilisateur ne peut pas consulter ses commandes concernant les évenements.

## Liens

[Maquette](https://www.figma.com/file/OJzplVihF5QOgiYCWDA68n/E-Commerce%3A-BANDO---Wireframe?node-id=0-1)
