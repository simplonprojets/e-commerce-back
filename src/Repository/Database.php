<?php

namespace App\Repository;

use PDO;

class Database
{
    public static function connect()
    {
        return new PDO('DATABASE_URL');
    }
}