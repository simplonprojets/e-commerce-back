<?php

namespace App\Repository;

use App\Entity\Artist;
use App\Entity\Genre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Artist>
 *
 * @method Artist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artist[]    findAll()
 * @method Artist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    public function save(Artist $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Artist $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByGenre(int $value): array {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT a, g
            FROM App\Entity\Artist a
            INNER JOIN a.genre g
            WHERE g.id = :value"
        )->setParameter("value", $value);

        //Methode doctrine (Jean)
        // ->andWhere('a.genre=:id')
        // ->setParameter('id', $value)
        // ->getQuery()
        // ->getResult();

        return $query->getResult();
    }

       public function findOneByWord(string $value): ?Artist
   {
       return $this->createQueryBuilder('a')
           ->andWhere('a.name LIKE :val')
           ->setParameter('val', '%'.$value.'%')
           ->getQuery()
           ->getOneOrNullResult()
       ;
   }

//    /**
//     * @return Artist[] Returns an array of Artist objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Artist
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
