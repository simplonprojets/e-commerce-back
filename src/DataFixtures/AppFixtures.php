<?php

namespace App\DataFixtures;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Event;
use App\Entity\EventOrder;
use App\Entity\Genre;
use App\Entity\MusicOrder;
use App\Entity\Song;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void

    {
        $faker = Factory::create();

        $genres = [
            "Rock",
            "Pop",
            "Rap",
            "Hip-Hop",
            "Metal",
            "K-Pop",
            "J-Pop",
            "Classique",
            "Jazz",
            "Folk",
            "Electro",
            "Alternatif",
            "Hardcore",
            "Dubstep",
            "Techno",
            "Psytrance",
            "Grindcore",
            "Grunge",
            "Punk",
            "Minimal",
            "House",
            "EBM",
            "Industrial",
            "Experimental",
            "Reggae",
            "Funk",
            "Jazz Fusion",
            "Pop Punk",
            "Rock Progressif",
            "Blues",
            "heavy Metal",
            "Hard rock",
            "Black Metal",
            "Viking Metal",
            "Death Metal",
            "Crust Punk",
            "War Metal",
            "Dark Metal",
            "Doom Metal",
            "Metal Médiéval",
            "Metal Alternatif",
            "Nu Metal",
            "Metal Chrétien",
            "Mathcore"
        ];

        $roles = [['ROLE_ADMIN'], ['ROLE_USER'], ['ROLE_ARTIST']];
    
        foreach ($genres as $genreName) {
            $genre = new Genre();
            $genre->setName($genreName);

            for ($i = 0; $i < 3; $i++) {
                $user = new User();
                $user->setUsername($faker->userName());
                $user->setEmail($faker->email());
                $user->setPassword($faker->password());
                //10% de chance qu'un user soit admin
                $isAdmin = $faker->boolean(10);
                if ($isAdmin == false) {
                    //25% de chance qu'un user soit artist si il n'est pas admin
                    $isArtist = $faker->boolean(25);
                }
                if ($isAdmin) {
                    $user->setRoles($roles[0]);
                } elseif ($isArtist) {
                    $user->setRoles($roles[2]);
                    //Si un user est un artist il faut créer une entité artist
                    $artist = new Artist();
                    $artist->setUserAccount($user);
                    $artist->setName($faker->name());
                    $artist->setDescription($faker->paragraph(1));
                    //On attribue un le genre qui vient d'etre crée a un artist
                    $artist->setGenre($genre);
                    $artist->setImg($faker->imageUrl());

                    // Boucle pour les events
                    for ($e = 0; $e < intval($faker->randomFloat(null, 0, 4)); $e++) {
                        $event = new Event();
                        $event->setName($faker->words(3, true));
                        $event->setDescription($faker->paragraph(1));
                        $event->setPrice(round($faker->randomFloat(null, 5, 70), 2));
                        $event->setDate($faker->dateTimeBetween("now", "+2 years"));
                        $event->setAddress($faker->streetAddress());
                        $littleEvent = $faker->boolean(80);
                        if ($littleEvent) {
                            // ceil permet d'arrondir a l'entier supérieur le plus proche
                            $littleCapacity = ceil(intval($faker->randomFloat(null, 30, 1000)) / 10) * 10;
                            $event->setCapacity($littleCapacity);
                        } else {
                            $bigCapacity = ceil(intval($faker->randomFloat(null, 1000, 5000)) / 10) * 10;
                            $event->setCapacity($bigCapacity);
                        }
                        $event->setImg($faker->imageUrl());
                        $event->setArtists($artist);

                        $manager->persist($event);

                        //Boucle EventOrder
                        $eventTicket = intval($faker->randomFloat(null, 15, $event->getCapacity()));
                        for ($f = 0; $f < $eventTicket; $f++) {
                            $eventOrder = new EventOrder();
                            $eventOrder->setDate($faker->dateTimeBetween("now", $event->getDate()));
                            $orderNumber = intval($faker->randomFloat(null, 1, 4));
                            $eventOrder->setNumber($orderNumber);
                            $eventTicket -= $orderNumber;
                            $eventOrder->setEvent($event);
                            $eventOrder->setUser($user);
                            $eventOrder->setPrice(round($faker->randomFloat(null, 3, 20), 2));

                            $manager->persist($eventOrder);
                        }
                    }

                    //Boucle pour création d'un ou plusieurs albums par artiste
                    for ($j = 0; $j < intval($faker->randomFloat(null, 1, 5)); $j++) {
                        $album = new Album();
                        /*Le titre de l'album fait entre 5 et 20 caracteres
                         * on utilise randomFloat pour générer un nombre selon un interval, puis 'intval' pour convertir en nombre entier
                         * rtrim ici permet de supprimer le "." généré par faker
                         */
                        $album->setTitle(rtrim($faker->text(intval($faker->randomFloat(null, 7, 10))), "."));
                        $album->setDate($faker->dateTimeThisDecade());
                        //$albumDescription = ;
                        $album->setDescription($faker->paragraph(1));
                        // On attribue le meme genre a l'album que celui de l'artist
                        $album->setGenre($artist->getGenre());
                        $album->setArtist($artist);
                        $album->setImg($faker->imageUrl());
                        $album->setPrice(round($faker->randomFloat(null, 5, 17), 2));

                        $hasOrdered = false;
                        //On parcourt les albums achetés, si un user en a déja acheté un il ne pourra pas le meme
                        foreach ($album->getMusicOrders() as $order) {
                            if ($order->getUser() === $user) {
                                $hasOrdered = true;
                                break;
                            }
                        }

                        if (!$hasOrdered) {
                            $musicOrder = new MusicOrder();
                            $musicOrder->setDate($faker->dateTimeBetween($album->getDate(), "now"));
                            $musicOrder->setPrice(round($faker->randomFloat(null, 3, 20), 2));
                            $musicOrder->setAlbum($album);
                            $musicOrder->setUser($user);
                            $manager->persist($musicOrder);
                        }

                        // Boucle pour création d'un ou plusieurs son pour un album
                        for ($k = 0; $k < intval($faker->randomFloat(null, 7, 15)); $k++) {
                            $song = new Song();
                            $song->setGenre($artist->getGenre());
                            $song->setDuration(round($faker->randomFloat(null, 1, 5), 2)); //Durée du son entre 1 et 5min
                            $song->setResource($faker->imageUrl());
                            $song->setTitle(rtrim($faker->text(intval($faker->randomFloat(null, 7, 20))), "."));
                            $song->setAlbum($album);

                            $manager->persist($song);
                        }
                        // On attribue l'abum qui vient d'etre crée a l'artiste
                        $artist->addAlbum($album);

                        $manager->persist($album);
                    }
                    $manager->persist($artist);
                    $user->setArtist($artist);
                } else {
                    $user->setRoles($roles[1]);
                }
                $manager->persist($user);
            }
            $manager->persist($genre);
        }

        // //Pour tester les routes d'un user connecté
        // $userTest = new User();
        // $userTest->setUsername("user-test");
        // $userTest->setEmail("test@test.com");
        // $hashedPassword = $passwordHasher->hashPassword(
        //     $userTest,
        //     "1234"
        // );
        // $userTest->setPassword($hashedPassword);
        // $userTest->setRoles($roles[1]);
        // $manager->persist($userTest);

        // //Pour tester les routes d'un admin connecté
        // $adminTest = new User();
        // $adminTest->setUsername("admin-test");
        // $adminTest->setEmail("admintest@test.com");
        // $hashedPassword = $passwordHasher->hashPassword(
        //     $adminTest,
        //     "1234"
        // );
        // $adminTest->setPassword($hashedPassword);
        // $adminTest->setRoles($roles[0]);
        // $manager->persist($adminTest);

        // //Pour tester les routes d'un artist connecté
        // $artistTest = new User();
        // $artistTest->setUsername("artist-test");
        // $artistTest->setEmail("artisttest@test.com");
        // $hashedPassword = $passwordHasher->hashPassword(
        //     $artistTest,
        //     "1234"
        // );
        // $artistTest->setPassword($hashedPassword);
        // $artistTest->setRoles($roles[3]);
        // $manager->persist($artistTest);

        $manager->flush();
    }
}
//On doit créer 3 users (admin, artist, user)
//Il faudra utiliser la commande pour générer un hash de mot de passe
//Et la commande pour générer un token JWT
//Ces users nous permettront de faire des test sur nos route protégés 