<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\User;
use App\Repository\ArtistRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class AuthController extends AbstractController
{
    public function __construct(private UserRepository $repo, private ArtistRepository $aRepo) {
    }

    #[Route('/api/user', methods: 'POST')]
    public function register(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher): Response
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');
        //On vérifie s'il n'y a pas déjà un user avec cet email en base de données
        if($this->repo->findOneBy(['username' => $user->getUsername()])) {
            return $this->json(['error' => 'User already exists'], Response::HTTP_BAD_REQUEST);
        }
        //On hash son mot de passe (symfony rajoute automatiquement sel et probablement poivre)
        $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hashedPassword);

        $roleArray = $user->getRoles();
    
        if (array_search("ROLE_ARTIST", $roleArray) !== false) {
            $artist = new Artist();
            $artist->setUserAccount($user);
            $artist->setName($user->getUsername());
            $this->aRepo->save($artist);
            $user->setArtist($artist);
        }

        $this->repo->save($user, true);

        return $this->json($user);
    }

    #[Route("/api/user/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher)
    {   
        // if () {
            $user = $this->getUser();
            try {
                $serializer->deserialize($request->getContent(), User::class, 'json', [
                    'object_to_populate' => $user
                ]);
                $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
                $user->setPassword($hashedPassword);
                $this->repo->save($user, true);
                return $this->json($user);

            } catch (ValidationFailedException $e) {
                return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
            } catch (NotEncodableValueException $e) {
                return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
            }
        // } else {
        //     return $this->json(null, Response::HTTP_UNAUTHORIZED);
        // }
    }


    #[Route('/api/account', methods: 'GET')]
    public function restrictedZone() {
        
        return $this->json($this->getUser());
    }
}

