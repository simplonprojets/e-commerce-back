<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/event')]
class EventController extends AbstractController
{

    public function __construct(private EventRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Event $event)
    {
        return $this->json($event);
    }

    #[Route("/a", methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $event = $serializer->deserialize($request->getContent(), Event::class, 'json');
            $this->repo->save($event, true);

            return $this->json($event, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/a/{id}", methods: 'DELETE')]
    public function delete(Event $event)
    {
        $this->repo->remove($event, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/a/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Event $event, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Event::class, 'json', [
                'object_to_populate' => $event
            ]);
            $this->repo->save($event, true);
            return $this->json($event);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route("/artist/{id}", methods: "GET")]
    public function byArtist(int $id):Response {

        $results = $this->repo->findByArtist($id);

        return $this->json($results); 
    }

}
