<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\Genre;
use App\Entity\User;
use App\Repository\ArtistRepository;
use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route("/api/artist")]
class ArtistController extends AbstractController
{
    public function __construct(private ArtistRepository $repo)
    {
    }

    #[Route(methods: "GET")]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route("/{id}", methods: "GET")]
    public function one(Artist $artist)
    {
        return $this->json($artist);
    }

    #[Route(methods: "POST")]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $artist = $serializer->deserialize($request->getContent(), Artist::class, 'json');
            $this->repo->save($artist, true);

            return $this->json($artist, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/a/{id}", methods: 'DELETE')]
    public function delete(Artist $artist)
    {
        // On verifie que l'user qui veux supprimer son compte artist est bien la bonne personne
        $artistId = $artist->getId();
        $user = $this->getUser();
        $userArtistId = $user->getArtist()->getId();
        if ($artistId == $userArtistId) {
            if ($artistId == $userArtistId) {
                $this->repo->remove($artist, true);
                return $this->json(null, Response::HTTP_NO_CONTENT);
            } else { // Sinon on renvoie un message d'erreur
                return $this->json(null, Response::HTTP_UNAUTHORIZED);
            }
        }
    }

    #[Route("/a/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Artist $artist, Request $request, SerializerInterface $serializer, GenreRepository $genreRepo)
    {   

        $artistId = $artist->getId();
        $user = $this->getUser();
        $userArtistId = $user->getArtist()->getId();
        if ($artistId == $userArtistId) {
            try {
                $serializer->deserialize($request->getContent(), Artist::class, 'json', [
                    'object_to_populate' => $artist
                ]);

                /* on convertit la requete en tableau
                ** dans ce tableau il y a une clef 'genre' qui contient l'id du genre
                ** On fais un findById avec l'id du genre */
                $data = $request->toArray();
                $genre = $genreRepo->find($data['genre']);
                $artist->setGenre($genre);

                $this->repo->save($artist, true);
                return $this->json($artist);

            } catch (ValidationFailedException $e) {
                return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
            } catch (NotEncodableValueException $e) {
                return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
            }
        } else {
            return $this->json(null, Response::HTTP_UNAUTHORIZED);
        }
    }

    #[Route("/genre/{genre}", methods: "GET")]
    public function genre(int $genre): Response
    {
        //Ne fonctionne qu'avec un résultat
        $results = $this->repo->findbyGenre($genre);

        return $this->json($results);
    }

    #[Route("/find/{word}", methods: "GET")]
    public function searchArtist(string $word)
    {
        $artist = $this->repo->findOneByWord($word);
        return $this->json($artist);
    }

}