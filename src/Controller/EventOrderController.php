<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\EventOrder;
use App\Repository\EventOrderRepository;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/eventorder')]
class EventOrderController extends AbstractController
{
    public function __construct(private EventOrderRepository $repo, private EventRepository $Erepo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(EventOrder $eventorder)
    {
        return $this->json($eventorder);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $eventorder = $serializer->deserialize($request->getContent(), EventOrder::class, 'json');
            $this->repo->save($eventorder, true);

            return $this->json($eventorder, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/{id}", methods: 'DELETE')]
    public function delete(EventOrder $eventorder)
    {
        $this->repo->remove($eventorder, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(EventOrder $eventorder, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), EventOrder::class, 'json', [
                'object_to_populate' => $eventorder
            ]);
            $this->repo->save($eventorder, true);
            return $this->json($eventorder);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route("/{id}/{number}",methods: 'POST')]
    public function addByEvent(Event $event, int $number, Request $request, SerializerInterface $serializer)
    {
        try {
            $eventorder=new EventOrder();
            $event->setCapacity($event->getCapacity()-$number);
            $this->Erepo->save($event, true);
            $eventorder->setEvent($event);
            $eventorder->setNumber($number);
            $eventorder->setPrice($event->getPrice() * $number);
            $eventorder->setDate(new \DateTime());
            $eventorder->setUser($this->getUser());
            $this->repo->save($eventorder, true);
            return $this->json($event, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}
