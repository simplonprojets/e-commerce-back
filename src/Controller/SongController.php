<?php

namespace App\Controller;

use App\Entity\Song;
use App\Repository\SongRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/song')]
class SongController extends AbstractController
{

    public function __construct(private SongRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Song $song)
    {

        return $this->json($song);
    }

    #[Route("/a", methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $song = $serializer->deserialize($request->getContent(), Song::class, 'json');
            $this->repo->save($song, true);

            return $this->json($song, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }


    #[Route("/a/{id}", methods: 'DELETE')]
    public function delete(Song $song)
    {
        $this->repo->remove($song, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route("/a/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Song $song, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Song::class, 'json', [
                'object_to_populate' => $song
            ]);
            $this->repo->save($song, true);
            return $this->json($song);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route("/album/{id}", methods: "GET")]
    public function byAlbum(int $id):Response {
        $results = $this->repo->findByAlbum($id);

        return $this->json($results); 
    }

}

