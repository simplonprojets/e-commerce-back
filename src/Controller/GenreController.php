<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/genre')]
class GenreController extends AbstractController
{

    public function __construct(private GenreRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Genre $genre)
    {

        return $this->json($genre);
    }

    #[Route("/admin", methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $genre = $serializer->deserialize($request->getContent(), Genre::class, 'json');
            $this->repo->save($genre, true);

            return $this->json($genre, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }


    #[Route("admin/{id}", methods: 'DELETE')]
    public function delete(Genre $genre)
    {
        $this->repo->remove($genre, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route("admin/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Genre $genre, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Genre::class, 'json', [
                'object_to_populate' => $genre
            ]);
            $this->repo->save($genre, true);
            return $this->json($genre);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

}