<?php

namespace App\Controller;

use App\Entity\MusicOrder;
use App\Entity\Album;
use App\Repository\AlbumRepository;
use App\Repository\MusicOrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/musicorder')]
class MusicOrderController extends AbstractController
{
    public function __construct(private MusicOrderRepository $repo,private AlbumRepository $alRepo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(MusicOrder $musicorder)
    {
        return $this->json($musicorder);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $musicorder = $serializer->deserialize($request->getContent(), MusicOrder::class, 'json');
            $this->repo->save($musicorder, true);

            return $this->json($musicorder, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/{id}", methods: 'DELETE')]
    public function delete(MusicOrder $musicorder)
    {
        $this->repo->remove($musicorder, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(MusicOrder $musicorder, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), MusicOrder::class, 'json', [
                'object_to_populate' => $musicorder
            ]);
            $this->repo->save($musicorder, true);
            return $this->json($musicorder);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }
    #[Route("/{id}",methods: 'POST')]
    public function addByAlbum(Album $album,Request $request, SerializerInterface $serializer)
    {
        try {
            $musicorder=new MusicOrder();
            $musicorder->setAlbum($album);
            $musicorder->setPrice($album->getPrice());
            $musicorder->setDate(new \DateTime());
            $musicorder->setUser($this->getUser());
            $this->repo->save($musicorder, true);
            return $this->json($musicorder, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

}
