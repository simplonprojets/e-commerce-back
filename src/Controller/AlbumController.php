<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\User;
use App\Entity\Genre;
use App\Repository\AlbumRepository;
use App\Repository\ArtistRepository;
use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route("/api/album")]
class AlbumController extends AbstractController
{
    public function __construct(private AlbumRepository $repo)
    {
    }

    #[Route(methods: "GET")]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route("/{id}", methods: "GET")]
    public function one(Album $album)
    {
        return $this->json($album);
    }

    #[Route("/a/{id}", methods: "POST")]
    public function add(Request $request, SerializerInterface $serializer, ArtistRepository $artistRepo, GenreRepository $genreRepo)
    {
        try {
            $album = $serializer->deserialize($request->getContent(), Album::class, 'json');
            
                $data = $request->toArray();
                $genre = $genreRepo->find($data['genre_id']);
                $album->setGenre($genre);
            
            /**
             * @var \App\Entity\User
             */
            $user = $this->getUser();
            
            $album->setArtist($user->getArtist());
            $this->repo->save($album, true);

            return $this->json($album, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/a/{id}", methods: 'DELETE')]
    public function delete(Album $album)
    {
        $this->repo->remove($album, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/a/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Album $album, Request $request, SerializerInterface $serializer)
    {
        try {
            $serializer->deserialize($request->getContent(), Album::class, 'json', [
                'object_to_populate' => $album
            ]);
            $this->repo->save($album, true);
            return $this->json($album);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/artist/{id}", methods: "GET")]
    public function byArtist(int $id):Response {
        $results = $this->repo->findByArtist($id);

        return $this->json($results); 
    } 
    
    #[Route('/{id}/like', methods: 'PATCH')]
    public function like(album $album)
    {
        if($album->getUsers()->contains($this->getUser())) {
            $album->removeUser($this->getUser());
        }else {
            $album->addUser($this->getUser());
        }
        $this->repo->save($album, true);
        return $this->json($album);
    }

}