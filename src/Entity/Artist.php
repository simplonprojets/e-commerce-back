<?php

namespace App\Entity;

use App\Repository\ArtistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: ArtistRepository::class)]
class Artist
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable:true)]
    private ?string $img = null;

    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'artists', targetEntity: Event::class, orphanRemoval: true)]
    private Collection $events;


    #[ORM\OneToMany(mappedBy: 'artist', targetEntity: Album::class, cascade: ['persist', 'remove'], fetch: 'EAGER')]
    private Collection $albums;


    #[ORM\ManyToOne(inversedBy: 'artist',  cascade: ['persist'], fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Genre $genre = null;

    #[Ignore]
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'artistFavorite')]
    private Collection $users;

    #[Ignore]
    #[ORM\OneToOne(mappedBy: 'artist', cascade: ['persist', 'remove'])]
    private ?User $userAccount = null;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->users = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums->add($album);
            $album->setArtist($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album)) {
            // set the owning side to null (unless already changed)
            if ($album->getArtist() === $this) {
                $album->setArtist(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Event>
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
            $event->setArtists($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getArtists() === $this) {
                $event->setArtists(null);
            }
        }

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addArtistFavorite($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeArtistFavorite($this);
        }

        return $this;
    }

    public function getUserAccount(): ?User
    {
        return $this->userAccount;
    }

    public function setUserAccount(?User $userAccount): self
    {
        // unset the owning side of the relation if necessary
        if ($userAccount === null && $this->userAccount !== null) {
            $this->userAccount->setArtist(null);
        }

        // set the owning side of the relation if necessary
        if ($userAccount !== null && $userAccount->getArtist() !== $this) {
            $userAccount->setArtist($this);
        }

        $this->userAccount = $userAccount;

        return $this;
    }
}
