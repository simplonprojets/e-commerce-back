<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Ignore;


#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $username = null;

    #[ORM\Column(length: 255)]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column]
    private array $roles = [];

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: EventOrder::class, fetch: "EAGER")]
    private Collection $eventOrders;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: MusicOrder::class, fetch: "EAGER")]
    private Collection $musicOrders;

    #[Ignore]
    #[ORM\ManyToMany(targetEntity: Album::class, inversedBy: 'users',fetch: "EAGER")]
    private Collection $albumFavorite;

    #[ORM\ManyToMany(targetEntity: Artist::class, inversedBy: 'users')]
    private Collection $artistFavorite;
    
    #[ORM\OneToOne(inversedBy: 'userAccount', cascade: ['persist', 'remove'], fetch: "EAGER")]
    private ?Artist $artist = null;

    public function __construct()
    {
        $this->eventOrders = new ArrayCollection();
        $this->musicOrders = new ArrayCollection();
        $this->albumFavorite = new ArrayCollection();
        $this->artistFavorite = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return $roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return Collection<int, EventOrder>
     */
    public function getEventOrders(): Collection
    {
        return $this->eventOrders;
    }

    public function addEventOrder(EventOrder $eventOrder): self
    {
        if (!$this->eventOrders->contains($eventOrder)) {
            $this->eventOrders->add($eventOrder);
            $eventOrder->setUser($this);
        }

        return $this;
    }

    public function removeEventOrder(EventOrder $eventOrder): self
    {
        if ($this->eventOrders->removeElement($eventOrder)) {
            // set the owning side to null (unless already changed)
            if ($eventOrder->getUser() === $this) {
                $eventOrder->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MusicOrder>
     */
    public function getMusicOrders(): Collection
    {
        return $this->musicOrders;
    }

    public function addMusicOrder(MusicOrder $musicOrder): self
    {
        if (!$this->musicOrders->contains($musicOrder)) {
            $this->musicOrders->add($musicOrder);
            $musicOrder->setUser($this);
        }

        return $this;
    }

    public function removeMusicOrder(MusicOrder $musicOrder): self
    {
        if ($this->musicOrders->removeElement($musicOrder)) {
            // set the owning side to null (unless already changed)
            if ($musicOrder->getUser() === $this) {
                $musicOrder->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getAlbumFavorite(): Collection
    {
        return $this->albumFavorite;
    }

    public function addAlbumFavorite(Album $albumFavorite): self
    {
        if (!$this->albumFavorite->contains($albumFavorite)) {
            $this->albumFavorite->add($albumFavorite);
        }

        return $this;
    }

    public function removeAlbumFavorite(Album $albumFavorite): self
    {
        $this->albumFavorite->removeElement($albumFavorite);

        return $this;
    }

    /**
     * @return Collection<int, Artist>
     */
    public function getArtistFavorite(): Collection
    {
        return $this->artistFavorite;
    }

    public function addArtistFavorite(Artist $artistFavorite): self
    {
        if (!$this->artistFavorite->contains($artistFavorite)) {
            $this->artistFavorite->add($artistFavorite);
        }

        return $this;
    }

    public function removeArtistFavorite(Artist $artistFavorite): self
    {
        $this->artistFavorite->removeElement($artistFavorite);

        return $this;
    }
    /**
     * Méthode qui sert à remettre à zéro les données sensibles dans l'entité pour ne pas les persistées
     * (par exemple si on avait un champ pour le mot de passe en clair différent du champ password)
     */
	public function eraseCredentials() {
        }
	/**
     * Méthode indiquant à Symfony Security l'identifiant du user, donc soit le mail, soit le username
     * soit le téléphone, soit le numéro de sécu etc.
     */
	public function getUserIdentifier(): string {
                return $this->username;
        }

    public function getArtist(): ?Artist
    {
        return $this->artist;
    }

    public function setArtist(?Artist $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

}
