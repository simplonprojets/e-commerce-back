<?php

namespace App\Entity;

use App\Repository\GenreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: GenreRepository::class)]
class Genre implements \JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'genre', targetEntity: Album::class)]
    private Collection $albums;

    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'genre', targetEntity: Song::class)]
    private Collection $song;

    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'genre', targetEntity: Artist::class)]
    private Collection $artist;

    public function __construct()
    {
        $this->albums = new ArrayCollection();
        $this->song = new ArrayCollection();
        $this->artist = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**

     * @return Collection<int, Album>
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums->add($album);
            $album->setGenre($this);

        }

        return $this;
    }


    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album)) {
            // set the owning side to null (unless already changed)
            if ($album->getGenre() === $this) {
                $album->setGenre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Song>
     */
    public function getSong(): Collection
    {
        return $this->song;
    }

    public function addSong(Song $song): self
    {
        if (!$this->song->contains($song)) {
            $this->song->add($song);
            $song->setGenre($this);
        }

        return $this;
    }

    public function removeSong(Song $song): self
    {
        if ($this->song->removeElement($song)) {
            // set the owning side to null (unless already changed)
            if ($song->getGenre() === $this) {
                $song->setGenre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Artist>
     */
    public function getArtist(): Collection
    {
        return $this->artist;
    }

    public function addArtist(Artist $artist): self
    {
        if (!$this->artist->contains($artist)) {
            $this->artist->add($artist);
            $artist->setGenre($this);
        }

        return $this;
    }

    public function removeArtist(Artist $artist): self
    {
        if ($this->artist->removeElement($artist)) {
            // set the owning side to null (unless already changed)
            if ($artist->getGenre() === $this) {
                $artist->setGenre(null);
            }
        }

        return $this;
    }
	/**
	 * Specify data which should be serialized to JSON
	 * Serializes the object to a value that can be serialized natively by json_encode().
	 * @return mixed Returns data which can be serialized by json_encode(), which is a value of any type other than a resource .
	 */
	public function jsonSerialize() {
        return [
            'id'=> $this->id,
            'name'=> $this->name
        ];
	}
}
