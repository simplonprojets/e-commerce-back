<?php declare(strict_types = 1);

namespace App\Serializer;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


class EntityDenormalizer implements DenormalizerInterface
{
  
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, $type, $format = null, $context = []):bool
    {
        return strpos($type, 'App\\Entity\\') === 0 && (is_numeric($data) || is_string($data));
    }

    /**
     * @inheritDoc
     */
    public function denormalize($data, $class, $format = null, array $context = []): mixed
    {
        
        return $this->em->find($class, $data);
    }
}
